import json

from resources.lib.const import LANG, STRINGS, ROUTE, COMMAND, FILTER_CONFIG_ITEMS_LANG, FILTER_CONFIG_CONDITION_ITEMS, \
    FILTER_CONFIG_CONDITION_LANG, COLOR, FILTER_CONFIG_OPERATOR_LANG, FILTER_CONFIG_ITEM_TYPE_VALUE_TYPE, \
    LANG_FILTER_CONFIG_ITEM_TYPE, FILTER_CONFIG_CONDITION
from resources.lib.gui.directory_items import CommandItem
from resources.lib.routing.router import router
from resources.lib.storage.sqlite import DB
from resources.lib.utils.elasticsearch import get_base_query, push_to_bool_query, get_query
from resources.lib.utils.kodiutils import get_string


class FilterRenderer:
    @property
    def can_add_separator(self):
        return not self.config_items[-1]['is_separator'] if len(self.config_items) else False

    @property
    def config_list_items(self):
        items = []
        for i, item in enumerate(self.config_items):
            items.append(
                self.separator_list_item(i, item) if item['is_separator'] else self.condition_list_item(i, item))

        return items

    def __init__(self):
        self.name = get_string(LANG.NEW_FILTER)
        self.icon = None
        self.config_items = []
        self._id = None

    def condition_list_item(self, index, config):
        title_prefix = get_string(FILTER_CONFIG_CONDITION_LANG[config['operator']]) if config.get('operator') else ''
        lang = LANG_FILTER_CONFIG_ITEM_TYPE.get(config['value'])
        value = get_string(lang) if lang else config['value']
        title = " ".join([
            STRINGS.BOLD.format(get_string(FILTER_CONFIG_ITEMS_LANG[config['type']])),
            STRINGS.COLOR.format(COLOR.LIGHTSKYBLUE, get_string(FILTER_CONFIG_OPERATOR_LANG[config['comparison']])),
            value
        ])
        if title_prefix:
            title = STRINGS.FILTERS_CONFIG_ITEM_TITLE.format(STRINGS.COLOR.format(COLOR.LIGHTSKYBLUE, title_prefix),
                                                             title)
        return CommandItem(title=title,
                           url=router.get_url(ROUTE.COMMAND, command=COMMAND.FILTERS_EDIT_CONDITION, index=index,
                                              skip_operator='0'))

    def separator_list_item(self, index, config):
        return CommandItem(title=get_string(FILTER_CONFIG_CONDITION_LANG[config['operator']]),
                           url=router.get_url(ROUTE.COMMAND, command=COMMAND.FILTERS_EDIT_CONDITION_GROUP, index=index))

    def reset(self):
        self.name = get_string(LANG.NEW_FILTER)
        self.icon = None
        self.config_items = []
        self._id = None

    def create_item(self, operator=None, config_type=None, comparison=None, config_value=None):
        index = len(self.config_items)
        item = {
            'type': config_type,
            'value': config_value,
            'operator': operator,
            'comparison': comparison,
            'is_separator': False,
            'value_type': FILTER_CONFIG_ITEM_TYPE_VALUE_TYPE[config_type] if config_type is not None else None
        }
        self.config_items.append(item)
        return index, item

    def remove_item(self, item):
        self.config_items.remove(item)

    def create_group(self, operator):
        index = len(self.config_items)
        item = self.config_items.append({'is_separator': True, 'operator': operator})
        return index, item

    def get_json(self):
        return json.dumps({
            'name': self.name,
            'icon': self.icon,
            'items': self.config_items,
        })

    def save(self):
        DB.FILTERS.add(self.get_json(), self._id)

    def load(self, item):
        config = json.loads(item[1])
        self._id = item[0]
        self.name = config["name"]
        self.icon = config["icon"]
        self.config_items = config["items"]

    @staticmethod
    def get_query(config_items):
        base = get_base_query()
        queries = [base]
        query = get_base_query()
        curr_base_query_operator = "and"
        is_or = False
        for i, item in enumerate(config_items):
            if item["is_separator"]:
                if item["operator"] == FILTER_CONFIG_CONDITION.NOT_OR or item["operator"] == FILTER_CONFIG_CONDITION.OR:
                    curr_base_query_operator = item["operator"]
                    query["bool"]["minimum_should_match"] = 1
                for q in queries:
                    push_to_bool_query(query["bool"], curr_base_query_operator, q)
                queries = []
                curr_base_query_operator = item["operator"]
                is_or = False
            else:
                q = get_query(item)
                next_item = None
                if len(config_items) > i + 1:
                    next_item = config_items[i + 1]
                if not is_or and not item["is_separator"] and next_item and (next_item["operator"] == FILTER_CONFIG_CONDITION.NOT_OR or next_item["operator"] == FILTER_CONFIG_CONDITION.OR):
                    is_or = True
                    if i > 0:
                        base = get_base_query()
                        queries.append(base)

                    base["bool"]["minimum_should_match"] = 1
                    push_to_bool_query(base["bool"], next_item["operator"], q)
                elif is_or:
                    push_to_bool_query(base["bool"], item["operator"], q)
                    is_or = next_item and (next_item["operator"] == FILTER_CONFIG_CONDITION.NOT_OR or next_item["operator"] == FILTER_CONFIG_CONDITION.OR)
                elif item["operator"]:
                    push_to_bool_query(base["bool"], item["operator"], q)
                else:
                    base = get_base_query()
                    queries.append(base)
                    push_to_bool_query(base["bool"], FILTER_CONFIG_CONDITION.AND, q)

        for q in queries:
            push_to_bool_query(query["bool"], curr_base_query_operator, q)
        return query
